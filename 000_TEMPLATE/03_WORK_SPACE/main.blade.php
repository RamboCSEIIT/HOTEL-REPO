@extends('aa_ServerPart.aa_WorkSpace.aa_base_homepage') 
 
@section('title') 
       Banasura Hill Valley Home Stay
@stop 
 
@section('cssBlock') 
@include('aa_ServerPart.aa_WorkSpace.<%= DNAME %>.aa_include.za_css') 
@stop

@section('content') 
     

   <div class="  container-fluid">
       <div class="row">
           <div class="col-sm-12 col-md-12">
                  @include('aa_ServerPart.aa_WorkSpace.<%= DNAME %>.aa_include.zc_navbar') 
           </div>
        
       </div>
          
    </div> 

   
         

  <br>
  <br>
          @include('aa_ServerPart.ab_body.<%= DNAME %>.body')    
 
           
     
     
 @stop
 
 
 @section('bottomJS') 
 
      @include('aa_ServerPart.aa_WorkSpace.<%= DNAME %>.aa_include.zb_javascript')   
     
 @stop 
 
 
  