@extends('aa_ServerPart.aa_WorkSpace.aa_base_page') 
 
@section('title')
    Tour Place
@stop 
 
@section('cssBlock') 
  @include('aa_ServerPart.aa_WorkSpace.ae_ADMIN_PAGE.aa_include.za_css') 
@stop

@section('content') 

    <div class="wrapper ">  
    @include('aa_ServerPart.aa_WorkSpace.ae_ADMIN_PAGE.aa_include.zc_side_panel')
      
    <div class="main-panel"> 
        
         @include('aa_ServerPart.ab_body.ae_ADMIN_PAGE.body')
    </div>    
        
 
                 

   
   </div>

      
 
           
     
     
 @stop
 
 
 @section('bottomJS') 
 
      @include('aa_ServerPart.aa_WorkSpace.ae_ADMIN_PAGE.aa_include.zb_javascript')   
     
 @stop 
 
 
  