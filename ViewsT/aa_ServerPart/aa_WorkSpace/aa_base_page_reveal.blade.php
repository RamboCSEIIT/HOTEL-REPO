
<!--  It says document type for browser as html 5 file
<meta name="viewport" content="width=device-width, initial-scale=1">

-->

<!DOCTYPE html>
<html lang="en">

<head>
    
    <!-- Font Awesome -->
  
    @yield('cssBlock')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
   
  <title>    @yield('title')  </title>
</head>

<body  id="body" data-spy="scroll"  data-offset="1"  class="impress-not-supported" >
   
    
    
 @yield('content')
 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
   

   
 
 @yield('bottomJS')
    
    
      
   
   
    
   
</body>

</html>
