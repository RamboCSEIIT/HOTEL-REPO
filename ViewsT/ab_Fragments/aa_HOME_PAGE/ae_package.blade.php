 
 @section('image_pkg') 





@foreach ($galleryPackage as  $index => $gallery_item)
 


<div class="item" style=""> 
    
    <div  class="card_model card_v">
         <div class="container">
        <div class="front" style="background-image: url({!!  $gallery_item->image_link !!})">
          <div class="inner">
            <p>{!!  $gallery_item->heading !!}</p>
             
          </div>
                                       <div class="text-center mb-3"><a class="card-link btn btn-primary">Price Details</a></div>

        </div>
        <div class="back">
            <div class="inner" >
      <table class="table" style="height:inherit;border-radius:20%;">
    <thead >
      <tr style="background:#ff0081;" class="text-center">
          <th class="font_heading " style="color:whitesmoke">{!!  $gallery_item->heading !!}</th>
     </tr>
    </thead>
    <tbody style="color:brown">
        
         
      <tr class="table-warning">
        <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-12">
                          {!!  $gallery_item->subheading !!}
                    </div>
                     
   
                </div>
                
            </div>
         
        
        </td>
        
        
        
      </tr>
      
     <tr class="table-warning">
        <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-12">
                                             @if( $gallery_item->tentative==1)
                           <div class="text-center mb-3"><a class="card-link btn btn-primary">Cost Tentative</a></div>
                       @else
                           
                           <div class="text-center mb-3"><a class="card-link btn btn-primary"><span>&#8377;</span> {!!  $gallery_item->cost !!} {!!  $gallery_item->payment_mode !!} </a></div>
                            
                       @endif
                    </div>

   
                </div>
                
            </div>
         
        
        </td>
        
        
        
      </tr>
        
    </tbody>
  </table>
  {{--            
                <ul>
   
                        <li>{!!  $gallery_item->subheading !!}</li>
                        <li>                       @if( $gallery_item->tentative==1)
                           <div class="text-center mb-3"><a class="card-link btn btn-primary">Cost Tentative</a></div>
                       @else
                           
                           <div class="text-center mb-3"><a class="card-link btn btn-primary"><span>&#8377;</span> {!!  $gallery_item->cost !!} {!!  $gallery_item->payment_mode !!} </a></div>
                            
                       @endif
</li>

    
                </ul>
--}}
              
           </div>
            
            
         </div>
      </div>

    </div>
  
</div>








@endforeach


@stop 



<div id="package" class="mt-5">
    <h2 class= "text-center"> <span class="font_package_heading"> Packages</span></h2><br>
    
    
         <div class="container mt-1">


            <div class="row">

                <div class="owl-carousel owl-theme">                    
                    @yield('image_pkg')                    

                </div>
            </div> 
              
             
              <div class="row justify-content-center">
    <div class="col-12 text-center">
        <button id="button_pkg" class="bubbly-button">Booking & Package Details</button>

      
    </div>
      </div>


        </div>

    


</div>
 