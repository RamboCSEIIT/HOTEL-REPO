 
@section('contactForm')

<form    method="POST" id="enquiryform" action="/contact" style="padding:0px 15px;">

    <div class="row">
        <div class="col-sm-12 form-group">
            <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 form-group">
            <input class="form-control" id="mobile" name="mobile" placeholder="mobile" type="number" required>
        </div>
        <div class="col-sm-6 form-group">
            <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
    </div>


    <textarea class="form-control" id="comments" name="comments"  placeholder="Comment" rows="5"></textarea><br>

    <div class="row">
        <div class="col-sm-12 form-group text-center">

            <button class="btn btn-default     centered button_round   " type="submit">Send</button>

        </div>
    </div>

</form>
@stop



@section('Weather')
<div class="jumbotron">
    
  <table class="table">
    <thead>
      <tr style="background:#ff0081;" class="text-center">
          <th class="font_heading " style="color:whitesmoke">Weather Forecast</th>
     </tr>
    </thead>
    <tbody style="color:brown">
        
      <tr class="table-danger ">
          <td class="text-center">
          
          <div id="date"></div>
        </td>
    
      </tr>
        
      <tr class="table-warning">
        <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                         <i class="fa fa-home" style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                       Company Name, Wayanad
                    </div>

                </div>
                
            </div>
         
        
        </td>
        
        
        
      </tr>
      
      
      <tr class="table-warning">
              <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                           <i class="wi wi-fog" style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                       <span id="weather-id" style=""></span>
                    </div>

                </div>
                
            </div>
         
        
        </td>
    
      </tr>
      
      
       <tr class="table-warning">
              <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                         <i class="wi wi-hot" style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                        Temperature: <span id="temperature"></span> &deg;<span id="toCelsius">C</span> 
 
                    </div>

                </div>
                
            </div>
         
        
        </td>
    
      </tr>  
      
       <tr class="table-warning">
              <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                         <i class="wi wi-windy " style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                        Wind Speed:   <span id="wind-speed"></span> 
                    </div>

                </div>
                
            </div>
         
        
        </td>
    
      </tr>  
      
<tr class="table-warning">
              <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                         <i class="wi wi-humidity" style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                        Humidity :   <span id="humidity"></span> 
                    </div>

                </div>
                
            </div>
         
        
        </td>
    
      </tr>  
      
    </tbody>
  </table>
   
       
    
  
    
    
    
</div>

@stop


@section('contact')
               <div class="row  justify-content-center mt-5" >
                    <div class="col-12">
                        
                        <h2 class="text-center">CONTACT</h2> 
                                                <div class="text-center">      <span id="contactmsg" class="company_moto_font " style="color: whitesmoke;background-color:maroon;font-size: 15px"></span>

                        </div>

 
                        @yield('contactForm') 

                    </div>
                </div>

@stop


<div id="contact" >



    <!-- Container (Contact Section) -->
    <div class="container" >


        <div class="row " >
            <div class="col-lg-5" style="padding-left: 20px;padding-right: 20px"  >
                @yield('Weather')
            </div>

            <div class="col-lg-7 jumbotron boarderLine " style="padding-left: 25px;padding-right: 25px" >
                
                     @yield('contact')
                
                

 
            </div>

        </div>


    </div>


</div>

