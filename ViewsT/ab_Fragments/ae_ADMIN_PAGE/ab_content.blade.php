<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="title text-center">Banasura Hill Valley Home Stay</h5>
                    <p class="category text-center">Makes Your Next Trip Unforgettable With Us
 </p>
                </div>
                <div class="card-block m-3">
                      <div class="container-fluid text-center">
        <div class="row">
            <br>
            <br>
        </div>
        <h2 class = "icon_font">SERVICES</h2>

        <br>
        <div class="row">
            <div class="col-sm-4">
                <i class="fa fa-columns" style="font-size:48px;color:red"></i>
                <h4 class="">HOTELS</h4>
                <p> We provide hotel booking as per the customer requirement. You can book through our site or via directly contacting our team</p>
            </div>
            <div class="col-sm-4">
                <i class="fa fa-text-width" style="font-size:48px;color:red"></i>
                <h4 class="">TOUR GUIDING</h4>
                <p> Experienced tour guide to assist your trip</p>
            </div>
            <div class="col-sm-4">
                <i class="fa fa-paragraph" style="font-size:48px;color:red"></i>
                <h4 class="">CUSTOMER CARE</h4>
                <p> We offer service which ensures customer satisfaction. Our team will be available any time including day and night on request. </p>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-sm-4">
                <i class="fa fa-strikethrough" style="font-size:48px;color:red"></i>
                <h4 class="">SECURITY</h4>
                <p>Security of our customer is utmost priority to us. We make sure that apt people will be available at your service on your trip. Proper monitoring of the hotel and travel amenities is done prior to the trip and also during the tour. </p>
            </div>
            
            
             <div class="col-sm-4">
                <i class="fa fa-undo" style="font-size:48px;color:red"></i>
                <h4 class="company_moto_font">Medical Emergency </h4>
                <p> In case of medical emergencies caused by health issues we will assist you to get the best medical facilities in Wayanad . You will be provided with our medical assistance any time </p>
            </div>
           
            
            <div class="col-sm-4">
                <i class="fa fa-table" style="font-size:48px;color:red"></i>

                <h4 class="">TRANSPORTATION</h4>
                <p> It can be arranged before or after the arrival of the tour. We will arrange travel amenities with properly maintained vechicles as your security is utmost concern for us</p>
            </div>
        </div>
    </div>

                </div>

            </div>


        </div>

    </div>


</div>
