        <footer class="footer" >
                    <div class="container-fluid">
                        <nav>
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="/register">Register</a></li>
                                <li><a href="/faq">Faq</a></li>
                                <li><a href="/login">Login</a></li>
                            </ul>
                        </nav>
                        <div class="copyright">
                            &copy; <script>document.write(new Date().getFullYear())</script>, Designed by <a href="#" target="_blank">Company Name</a>. Coded by <a href="#" target="_blank">Name</a>.
                        </div>
                    </div>
                </footer>
