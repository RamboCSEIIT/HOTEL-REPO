<?php
use Illuminate\Database\Capsule\Manager as DB;

 // echo "Inside root";
    
   $router->map('GET', '/tour-places', 'tour\Controllers\PageController@getShowTourPlacesPage', 'tour_places');
   $router->map('GET', '/crud-test', 'tour\Controllers\zd_curd_pageController@getShow_zd_curd_page', 'crud_test');
   $router->map('POST', '/crud-test', 'tour\Controllers\zd_curd_pageController@postShow_zd_curd_page', 'crud_post');

   $router->map('GET', '/package', 'tour\Controllers\PackageController@getShowPackagePage', 'package');
   $router->map('GET', '/register', 'tour\Controllers\RegisterController@getShowRegisterPage', 'register');
   $router->map('POST', '/register', 'tour\Controllers\RegisterController@postShowRegisterPage', 'register_post');
   $router->map('GET', '/verify-account', 'tour\Controllers\RegisterController@getVerifyAccount', 'verify_account');

   $router->map('POST', '/booking-from_list', 'tour\Controllers\zc_BookingController@postShowBookingPageList', 'book_post_List');
   $router->map('POST', '/booking', 'tour\Controllers\zc_BookingController@postShowBookingPage', 'book_post');
  // $router->map('GET', '/booking', 'tour\Controllers\zc_BookingController@getShowBookingPage', 'book_get');
 
   
   
   $router->map('GET', '/crud-test', function()
     {
         
        // phpinfo();
     //  dd("hello");
         
     });
 


   
   if (tour\auth\LoggedIn::user()) 
   {
       
      $router->map('GET', '/add-enquiry', 'tour\Controllers\EnquiryController@getShowAdd', 'add_enquiry');
      $router->map('POST', '/add-enquiry', 'tour\Controllers\EnquiryController@postShowAdd', 'add_enquiry_post');
 
       
   }
   
   
   // admin routes
//  if (tour\auth\LoggedIn::user() && tour\auth\LoggedIn::user()[0]->access_level == 2)
  {
      
           
    $router->map('GET', '/admin-panel', 'tour\Controllers\zb_AdminPanelController@getShowAdminPanelPage', 'show_admin_panel');
   
  }

   //re 
   $router->map('GET', '/login', 'tour\Controllers\AuthenticationController@getShowLoginPage', 'login');
   
   $router->map('POST', '/login', 'tour\Controllers\AuthenticationController@postShowLoginPage', 'login_post');
  
   $router->map('GET', '/logout', 'tour\Controllers\AuthenticationController@getLogout', 'logout');
 
 
     
   //routes
   //$router->map('GET', '/', 'tour\Controllers\PageController@getShowHomePage', 'home');
  // $router->map('GET', '/faq', 'tour\Controllers\PageController@getShowFaq_D', 'faq');
   
   //#
   $router->map('GET', '/', 'tour\Controllers\za_HomePageController@getShowHomePage', 'homeBootstrap');
   $router->map('GET', '/t', 'tour\Controllers\za_HomePageController@getShowHomePageT', 'homeBootstrapT');

   $router->map('POST', '/subs', 'tour\Controllers\za_HomePageController@postSubsHomePage', 'homesubs');
   $router->map('POST', '/contact', 'tour\Controllers\za_HomePageController@postContactHomePage', 'contact');
   $router->map('GET', '/hit', 'tour\Controllers\za_HomePageController@getHit', 'hit');
     
   $router->map('GET', '/[*]', 'tour\Controllers\PageController@getShowPage', 'generic_page');
 
     
   
     
 

?>
