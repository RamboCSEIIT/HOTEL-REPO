<?php


use Phinx\Migration\AbstractMigration;

class CreatePage extends AbstractMigration
{
     public function up()
    { 
         /*
          $pages = $this->table('pages');
         $pages->addColumn('browser_title', 'string')
             ->addColumn('page_content', 'text')
             ->addColumn('slug', 'string', ['default' => ''])
             ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
             ->addColumn('updated_at', 'datetime', ['null' => true])
             ->addIndex(['slug'], ['unique' => true])
             ->save();*/
           
         $this->execute("
             CREATE TABLE `pages` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `browser_title` varchar(255) NOT NULL,
                                    `page_content` text NOT NULL,
                                    `slug` varchar(255) NOT NULL DEFAULT '',
                                    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                    `updated_at` datetime DEFAULT NULL,
                                    `page_name` varchar(100) DEFAULT '',
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `slug_name` (`slug`)
                                                                    ) 
        ");   
        
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        //$this->dropTable('pages');
 
          
               $this->execute(" 
            DROP TABLE pages
         "); 

    }
}
