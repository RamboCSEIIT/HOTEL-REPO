<?php


use Phinx\Migration\AbstractMigration;

class CreateSubscription extends AbstractMigration
{
         //https://www.info.teradata.com/HTMLPubs/DB_TTU_16_00/index.html#page/Database_Management/B035-1094-160K/fti1472240591762.html  
     public function up()
    {
                 $this->execute("
                                    CREATE TABLE `Subscription` (
                                                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                                                   `email` varchar(1055) NOT NULL,
                                                                   `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                                                   `updated_at` datetime DEFAULT NULL,
                                                                    `status` int(11) NOT NULL DEFAULT 0,
                                                                    PRIMARY KEY (`id`)
                                                         ) 
        ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        
          
        $this->execute(" 
            DROP TABLE Subscription
         ");
    }

}
