<?php namespace  tour\Controllers;


use tour\Validation\Validator;
use duncan3dc\Laravel\BladeInstance;
use Illuminate\Database\Capsule\Manager as DB;
use tour\auth\LoggedIn;
class AuthenticationController extends BaseController
{

    protected $signer='';
   
    
    public function getShowLoginPage( ) 
    {
           
           echo $this->blade->render('aa_ServerPart.aa_WorkSpace.ab_LOGIN_PAGE.login_page', [
           
            'page_name' => '#login-page',
               'signer' => $this->signer
        ]);           
        
    }
    public function postShowLoginPage()
    {  
         if (!$this->signer->validateSignature($_POST['_token']))
        {
           $_SESSION['msg'] = ["Insecure login!"];
            echo $this->blade->render("aa_ServerPart.aa_WorkSpace.ab_LOGIN_PAGE.login_page", [
                'signer' => $this->signer,
                'page_name' => '#login-page'
            ]);
            unset( $_SESSION['msg']);
            exit();
        }
         


        
        
       // dd("Admin");
        
          $test =[];
        
          $message = '';
          $okay = false;
         // dd($okay);

          unset($_SESSION['msg']);
          unset($_SESSION['success']);
          $test[0]=$okay;
          $test[1]=$email = $_REQUEST['email_name'];
          $test[2]=$password = $_REQUEST['password_name'];
          $test[3]=$_usertype=$_REQUEST['_usertype'];
          
          
          
          if ( !strcmp($_usertype, "user" ) )
          {
             // echo "User man <br>";
             // dd($test);
              
                    $user =  DB::select('SELECT * FROM users WHERE email = :email AND access_level=:access_level ',
                                                        array(
                                                                 'email'  => $email,
                                                                 'access_level'=>1
                                                             )
                                                  );
 
              
          } 
          else 
          {
             // echo "Admin man <br>";
             // dd($test);
              
                                 $user =  DB::select('SELECT * FROM users WHERE email = :email AND access_level=:access_level',
                                                        array(
                                                                 'email'  => $email,
                                                                 'access_level'=>2
                                                             )
                                                  );
 
     
          }
               
      
          //dd($user);
          
    
        //  var_dump($okay.$email.$password);
          
        if ($user != null) 
        {
            // validate credentials
            if ( password_verify($password, $user[0]->password)) 
            //if ( !strcmp($password, $user[0]->password)) 
            {
               
                               
                if ($user[0]->active !=1) 
                {
                       $okay = false;
                       $message= $message."You have to verify the account . Please check your mail box";
       
                }
                else 
                {
                       $okay = true;
                       $message= $message."Password match ";
    
                }
            }
            else
            {
                
                                   
                $okay = false;
                $message= $message."<br> login mismatch";
  
                     

            }
        } 
         
        
      // var_dump($okay."::".$message);
    //   dd($okay);
        
        //dd($message);
        if ($okay) 
        {
            $_SESSION['user'] = $user;
              $_SESSION['success'] = [$message];
            header("Location: /");
              unset($_SESSION['success']);
            exit();
        } 
        else 
        {
            
           
            $_SESSION['msg'] = ["Invalid login!".$message];
            echo $this->blade->render("aa_ServerPart.aa_WorkSpace.ab_LOGIN_PAGE.login_page", [
                'signer' => $this->signer,
                'page_name' => '#login-page'
            ]);
            unset($_SESSION['msg']);
            exit(); 
        }


          
        
    }
    
    public function postShowLoginPageUser()
    {
  
        // echo "posted";
         $message = '';
         
         unset($_SESSION['msg']);
         
          $okay = true;
          $email = $_REQUEST['email_name'];
          $password = $_REQUEST['password_name'];
          
          
          $user =  DB::select('SELECT * FROM users WHERE email = :email',
                                                        array(
                                                                 'email'  => $email
                                                             )
                                                  );
/*          
          
        if ($user != null) 
        {
            // validate credentials
            if ( !password_verify($password, $user[0]->password)) 
            {
                $okay = false;
                $message= $message."<br>User exists but password miss match";
            }
            else
            {
                
                    if ($user[0]->active==0) 
                    {

                            $okay = false;
                            $message= $message."<br> You have not verified your registration<br> Check your mail inbox for registration verification";

                    }

            }
        } 
        else 
        {
            $okay = false;
            $message= $message."<br>No user exists with".$email;
        }
        
        
     
        
        
        
        if ($okay) 
        {
            $_SESSION['user'] = $user;
            header("Location: /");
            exit();
        } 
        else 
        {
            
           
            $_SESSION['msg'] = ["Invalid login!".$message];
            echo $this->blade->render("login_page", [
                'signer' => $this->signer,
                'page_name' => '#login-page'
            ]);
            unset($_SESSION['msg']);
            exit(); 
        }

*/

       
 
    }

 


    public function getLogout()
    {
        unset($_SESSION['user']);
        session_destroy();
        header("Location: /login");
        exit();
    }
    
    public function getTestUser()
    {

        dd(LoggedIn::user());
    }

}
?>