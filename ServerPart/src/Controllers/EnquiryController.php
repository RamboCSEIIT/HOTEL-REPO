<?php
namespace tour\Controllers;

use duncan3dc\Laravel\BladeInstance;
 
use tour\Validation\Validator;
use tour\auth\LoggedIn;
use Illuminate\Database\Capsule\Manager as DB;
use tour\email_send\Semail;
class EnquiryController extends BaseController
{

   
    public function getShowEnquiry()
    {
     
        
                  $Enquiry =  DB::select('SELECT * FROM Enquiry ORDER BY created_at DESC');                  
                  //dd($Enquiry);
                  
                          echo $this->blade->render('Enquiry', [
                              'enquiries' => $Enquiry,
                              'page_name' => '#show-entry-page'
        ]);

 
    }
    
                                      

    public function getShowAdd()
    {
          echo "Inside get show Enquiry";
        
        
        
         echo $this->blade->render('add-enquiry',
                  [
                    'page_name' => '#add-entry-page'
                   
                  ]
                 );
         
    }

    public function postShowAdd()
    {
        
        
        $errors = [];

        $validation_data = [
          'title' => 'min:3',
          'enquiry' => 'min:10',
        ];
        
        

        // validate data
        $validator = new Validator();

        $errors = $validator->isValid($validation_data);

        // if validation fails, go back to register
        // page and display error message

        if (sizeof($errors) > 0)
        {
            $_SESSION['msg'] = $errors;
           // echo $this->blade->render('add-testimonial');
            
            echo $this->blade->render('add-enquiry',
                  [
                    'page_name' => '#add-entry-page'
                  ]
                 );
            unset($_SESSION['msg']);
            exit();
        }
        
        
        DB::statement('INSERT INTO Enquiry (enquiry,title, user_id) VALUES (:enquiry,:title, :user_id)',
                                    array(
                                            'enquiry'     => $_REQUEST['enquiry'] ,
                                            'user_id'     =>LoggedIn::user()[0]->id,
                                            'title'     => $_REQUEST['title']
                                         )
                     );
        
       $message = $this->blade->render('aa_WorkSpace.emails.enquiry-email',
            [  'name' => LoggedIn::user()[0]->first_name ,
               'mobile' => LoggedIn::user()[0]->mob_number,
                'email' => LoggedIn::user()[0]->email,
                'message'=>$_REQUEST['enquiry']
               
               
             ]
        );
               
   
        Semail::_semail(getenv('ENQ_DEST_HOST'), "new Enquiry", $message);
   
       
        
         header("Location: /enquiry-saved");


    }
    
    public function postSubS()
    {
        
  /*      
        $errors = [];

        $validation_data = [
          'title' => 'min:3',
          'enquiry' => 'min:10',
        ];
        
        

        // validate data
        $validator = new Validator();

        $errors = $validator->isValid($validation_data);

        // if validation fails, go back to register
        // page and display error message

        if (sizeof($errors) > 0)
        {
            $_SESSION['msg'] = $errors;
           // echo $this->blade->render('add-testimonial');
 
            
            echo $this->blade->render('add-enquiry',
                  [
                    'page_name' => '#add-entry-page'
                  ]
                 );
            unset($_SESSION['msg']);
            exit(); 
        }
        else
        {
            
        }
        
        
        DB::statement('INSERT INTO Enquiry (enquiry,title, user_id) VALUES (:enquiry,:title, :user_id)',
                                    array(
                                            'enquiry'     => $_REQUEST['enquiry'] ,
                                            'user_id'     =>LoggedIn::user()[0]->id,
                                            'title'     => $_REQUEST['title']
                                         )
                     );
        
       $message = $this->blade->render('emails.enquiry-email',
            [  'name' => LoggedIn::user()[0]->first_name ,
               'mobile' => LoggedIn::user()[0]->mob_number,
                'email' => LoggedIn::user()[0]->email,
                'message'=>$_REQUEST['enquiry']
               
               
             ]
        );
               
   
        Semail::_semail(getenv('ENQ_DEST_HOST'), "new Enquiry", $message);
   
       
        
         header("Location: /enquiry-saved");
*/

    }
    
    
}
