<?php

namespace tour\Controllers;

use tour\Validation\Validator;
use duncan3dc\Laravel\BladeInstance;
use Illuminate\Database\Capsule\Manager as DB;
use tour\auth\LoggedIn;
use tour\email_send\Semail;

class RegisterController extends BaseController {

    ///  




    public function getShowRegisterPage() {
        // include (__DIR__."/../../Views/register_page.html");
        echo $this->blade->render('aa_ServerPart.aa_WorkSpace.ac_REGISTER_PAGE.register_page', [
            'page_name' => '#register-page',
            'signer' => $this->signer
        ]);
    }

    public function postShowRegisterPage() { 
        
       //  if (!$this->signer->validateSignature(  htmlspecialchars($this->signerT->getSignature())))
       if (!$this->signer->validateSignature($_POST['_token']))
        {
              $_SESSION['msg'] = ["Insecure Registration!"];
             echo $this->blade->render('aa_ServerPart.aa_WorkSpace.ac_REGISTER_PAGE.register_page', [
           
             'page_name' => '#register-page',
                  'signer' => $this->signer
        ]); 
           unset( $_SESSION['msg']);
            exit();
        }


        $errors = [];
        unset($_SESSION['msg']);
        
        $success = [];
        unset($_SESSION['success']);

        $validation_data = [
            'first_name_name' => 'min:3',
            'last_name_name' => 'min:3',
            'email_name_name' => 'email|equalTo:verify_email_name_name|unique:users',
            'password_name' => 'min:3|equalTo:verify_password_name'
        ];

        // validate data
        $validator = new Validator();

        $errors = $validator->isValid($validation_data);

        if (sizeof($errors) > 0) {

            // dd($_SESSION['msg']);

            $_SESSION['msg'] = $errors;
            //   header("Location: /register");
            // Blade cant take SESSION


            echo $this->blade->render('aa_ServerPart.aa_WorkSpace.ac_REGISTER_PAGE.register_page', [
                'page_name' => '#register-page',
                'signer' => $this->signer
            ]);

            unset($_SESSION['msg']);
            exit();
        }

        $row = DB::select('SELECT id FROM users WHERE email = :email', array(
                    'email' => $_REQUEST['email_name_name']
                        )
        );


        if ($row != null) {
            $errors[0] = "This email id is already used";

            $_SESSION['msg'] = $errors;
            //   header("Location: /register");
            // Blade cant take SESSION


            echo $this->blade->render('aa_ServerPart.aa_WorkSpace.ac_REGISTER_PAGE.register_page', [
                'page_name' => '#register-page',
                'signer' => $this->signer
            ]);

            unset($_SESSION['msg']);
            exit();
        }


        DB::statement('INSERT INTO users (first_name, last_name, email,password,mob_number) VALUES (:first_name, :last_name,:email,:password,:mob_number)', array(
            'first_name' => $_REQUEST['first_name_name'],
            'last_name' => $_REQUEST['last_name_name'],
            'email' => $_REQUEST['email_name_name'],
            'password' => password_hash($_REQUEST['password_name'], PASSWORD_DEFAULT),
            'mob_number' => (string) $_REQUEST['mob_number_name'])
        ); 
        
        $row = DB::select('SELECT id FROM users WHERE email = :email', array(
                    'email' => $_REQUEST['email_name_name']
                        )
        );


        $token = md5(uniqid(rand(), true)) . md5(uniqid(rand(), true));

        DB::statement('INSERT INTO users_pending (token, user_id) VALUES (:token, :user_id)', array(
            'token' => $token,
            'user_id' => $row[0]->id
                )
        );
        
        
        $message = $this->blade->render('aa_ServerPart.aa_WorkSpace.emails.welcome-email',
            ['token' => $token,
               'signer' => $this->signer]
        );
               
   
        Semail::_semail($_REQUEST['email_name_name'], "Welcome to Wayanadtours and Travels", $message);
       
        $success[0]=" You cant login in to our site with out email verification";

        $_SESSION['success'] =$success;
           //   header("Location: /register");
            // Blade cant take SESSION
     
                
             echo $this->blade->render('aa_ServerPart.aa_WorkSpace.ac_REGISTER_PAGE.register_page', [
           
            'page_name' => '#register-page',
                  'signer' => $this->signer
        ]); 
           
        unset( $_SESSION['success']);
        exit();



        //var_dump($errors);
        // dd("error ::000");
    }

    public function getVerifyAccount() {
           echo "inside verify account";
        
        $user_id = 0;
        $token = $_GET['token'];
        
       // $token = "31cb6adfc9bcff31f8ebded5bb44ba64bae8ec535625a82b512c95b9c6a4b553";
        
        
        $results =  DB::select('SELECT user_id FROM users_pending WHERE token = :token',
                                                        array(
                                                                 'token'  =>  $token
                                                             )
                                                  );
        
        

        if(count($results)>0)
        {
               
            DB::statement('  
                
                            UPDATE users
                            SET active=:active
                            WHERE id=:user_id
                     ',
                    
                            array(
                                     'active'  =>  1,
                                     'user_id'=>$results[0]->user_id
                                      
                                 )

                    );
          
            
            DB::statement('
                                DELETE FROM users_pending
                                WHERE token = :token', 
                                array(
                                       'token'  =>  $token
                                     )
                           );
  
               header("Location: /account-activated");
             //    dd( $results );
               exit();
           }
        else 
        {
                header("Location: /page-not-found");
                exit();
        }

        
         

    }

    
   


    

}

?>
