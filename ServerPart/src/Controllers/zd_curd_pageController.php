<?php

namespace tour\Controllers;

use tour\Validation\Validator;
//authors decided file level directives
use duncan3dc\Laravel\BladeInstance;
use Illuminate\Database\Capsule\Manager as DB;
use tour\email_send\Semail;
use tour\auth\LoggedIn;
use Illuminate\Http\Request;
use Assetic\Asset\AssetCollection;
use Assetic\Asset\FileAsset;
use Assetic\Asset\GlobAsset;

class zd_curd_pageController extends BaseController {

    public function getShow_zd_curd_page() {
$root = $_SERVER['DOCUMENT_ROOT'];

$path = '000_IMAGES';

    function getImagesFromDir($path) {
    $images = array();
    if ( $img_dir = @opendir($path) ) {
        while ( false !== ($img_file = readdir($img_dir)) ) {
            // checks for gif, jpg, png
            if ( preg_match("/(\.gif|\.jpg|\.png)$/", $img_file) ) {
                $images[] = $img_file;
            }
        }
        closedir($img_dir);
    }
    return $images;
}

function getRandomFromArray($ar) {
    mt_srand( (double)microtime() * 1000000 ); // php 4.2+ not needed
    $num = array_rand($ar);
    return $ar[$num];
}
$name=[];
$name=explode("/public",$root);
//dd($name);
//dd($root);
// Obtain list of images from directory 
$imgList = getImagesFromDir($name[0] . "/".$path);
//dd($imgList);
//dd($name[0] . "/".$path);
$img = getRandomFromArray($imgList);
//dd($img);
        //$url =  asset('../ASSETIC/package.js');
        //  dd($url);
     
       //  echo base_url();
        $code = $this->blade->render('aa_ServerPart.aa_WorkSpace.ZD_CURD_PAGE.main', [
            'page_name' => '#anyName',
            'img' => $img,
            'path'=> ''
        ]);

 




      echo $code;


        exit();


        // echo $code;
    }

    public function postShow_zd_curd_page() 
    {
        $target_dir = "../UPLOADS/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
// Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
// Check file size
        if ($_FILES["fileToUpload"]["size"] > 5000000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "The file " . basename($_FILES["fileToUpload"]["name"]) . " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }

}

?>
