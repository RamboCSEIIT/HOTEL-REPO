<?php  namespace tour\Controllers;
//authors decided file level directives
use duncan3dc\Laravel\BladeInstance;
use Illuminate\Database\Capsule\Manager as DB;

use voku\helper\HtmlMin;
class PageController extends BaseController 
{
    
    
    
    
    
    
    public function getShowHomePage_D() 
    {
         
      
         
          $galleryWayanad =  DB::select('SELECT * FROM galleryWayanad WHERE status = :status',
                  array(
                                                                 'status'  => 0
                                                             ));
          
          $galleryProfile =  DB::select('SELECT * FROM galleryProfile WHERE status = :status',
                  array(
                                                                 'status'  => 0
                                                           ));
          
         $galleryPackage =  DB::select('SELECT * FROM galleryPackage');
         //dd($galleryPackage);
          
         /*   $galleryWayanad =  DB::select('SELECT * FROM galleryWayanad WHERE status = :status',
                  array(
                                                                 'status'  => 0
                                                             ));*/
             
          
          //$galleryWayanad =  DB::select('SELECT * FROM galleryWayanad ORDER BY created_at DESC');
           
          //dd($galleryWayanad);
        
        $code = $this->blade->render('aa_WorkSpace.aa_homepage.ac_home_page', [
                                                             'page_name' => '#home-page',
                                                              'galleryWayanad' =>   $galleryWayanad, 
                                                               'galleryProfile' =>   $galleryProfile,
                                                                'galleryPackage'=>$galleryPackage
                                                          ]);
        
        
/*             
        if(!strcmp($_SESSION['TEST_MINIFY'] ,'1'))
        {
                 
                // dd($_SESSION['TEST_MINIFY']);
                $code = $this->blade->render('aa_WorkSpace.ac_blade.af_php_minify', [
                                                             'page_name' => '#Minify-page',
                                                              'code' =>   $code 
                                                          ]);
                
        }
 */       
        
     
         echo $code;
        
       //  echo str_replace(array("\r", "\n"), '', $code); // '<html> <body>à</body> </html>'
        
 
            
        
         
        
    }
  
    
    public function getShowHomePage() 
    {
         
       
      
       echo $this->blade->render('home_page', [
                                                             'page_name' => '#home-page'
                                                         
                                                          ]); 
  
       
        
    }
    
    
     public function getShowFaq_D() 
    {
              
    
          $Enquiry =  DB::select('SELECT * FROM Faq ORDER BY created_at DESC');                  
                  //dd($Enquiry);
  /* 
                          echo $this->blade->render('Enquiry', [
                              'enquiries' => $Enquiry,
                              'page_name' => '#show-entry-page'
        ]);

         */
        
    
         
         //dd($Enquiry);
        
        
        $code = $this->blade->render('aa_WorkSpace.ac_blade.ag_FAQ_page', [
                                                             'page_name' => '#faq-page',
                                                               'enquiries' => $Enquiry
            
                                                              
                                                          ]);
        
        
 
        if(!strcmp($_SESSION['TEST_MINIFY'] ,'1'))
        {
                 
                
                $code = $this->blade->render('aa_WorkSpace.af_php_minify', [
                                                             'page_name' => '#Minify-page',
                                                              'code' =>   $code 
                                                          ]);
                
        }
        
        
     
         echo $code;
        
       //  echo str_replace(array("\r", "\n"), '', $code); // '<html> <body>à</body> </html>'
        
 
            
        
         
        
    }
     
    
    
     public function getShowTourPlacesPage() 
    {
         
                       $galleryPackage = DB::select('SELECT * FROM galleryPackage');


         
       //dd("Hello");   
      // include (__DIR__."/../../Views/register_page.html");
        echo $this->blade->render('aa_ServerPart.aa_WorkSpace.af_TOUR_PLACES_PAGE.tourplaces_page', [
            'page_name' => '#package-page',
            'signer' => $this->signer,
             'galleryPackage' => $galleryPackage
        ]);
   
    }
    
    public function getShowPage() 
    {
         
       
     // echo $this->twig->render('home_page.html');
        
        //echo $this->blade->render('home_page');
       // echo "foo";
        
        $browser_title = "";
        $page_content  = "";
        $page_name     = "";
        $page_id = 0;

        // extract page name from the url
        $uri = explode("/", $_SERVER['REQUEST_URI']);
        $target = $uri[1];
        
                               $page =  DB::select('SELECT * FROM pages WHERE slug = :slug_name',
                                                        array(
                                                                 'slug_name'  => $target
                                                             )
                                                  );
                          //     dd($page);
  

        // find matching page in the db
       // $page = Page::where('slug', '=', $target)->get();
        

        // look up page content// finding out no page hit
        foreach ($page as $item)
        {
            $browser_title = $item->browser_title;
            $page_content = $item->page_content;
            $page_id = $item->id;
            $page_name= $item->page_name;
          //  echo $browser_title;
        }

       
        if (strlen($browser_title) == 0) 
        {
             header("Location: /page-not-found");
             exit();
        }
        // pass content to some blade template
        echo $this->blade->render('aa_ServerPart.aa_WorkSpace.generic.generic-page', [
            'browser_title' => $browser_title,
            'page_content' => $page_content,
            'page_id' => $page_id,
            'page_name' => '#'.$page_name
        ]);
        
         exit();
    }
    
    
    
    
    
}

?>
