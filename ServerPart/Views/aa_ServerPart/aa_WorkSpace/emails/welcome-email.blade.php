@extends('aa_ServerPart.aa_WorkSpace.emails.base-email')

@section('body')
<p>
    Welcome to Company Name!
</p>

 
<p>
    Please <a href="{!! getenv('HOST') !!}/verify-account?token={!! $token !!}">click here to activate</a> your account.
@stop
