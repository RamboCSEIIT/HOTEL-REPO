$(document).ready(function() {
    $('#registration_form').validate({ // initialize the plugin
        rules: {
            verify_email_name_name: {
                required: true,
                email: true,
                equalTo: "#email_name_id"
            },
            password_name: {
                required: true,
                minlength: 3
            },
            verify_password_name: {
                required: true,
                minlength: 3,
                equalTo: "#password_id"
            },
            first_name_name: {
                required: true,
                minlength: 3
            },
            last_name_name: {
                required: true,
                minlength: 3
            }
        }
    });
});
//.outerHeight(true) 
$(window).on('load', function() {
    var height = $("#regid").outerHeight(true);
    $("#particles-js").css("height", height);
    // ParticlesJS Config.
    particlesJS("particles-js", {
        "particles": {
            "number": {
                "value": 80,
                "density": {
                    "enable": true,
                    "value_area": 400
                }
            },
            "color": {
                "value": "#ff8c00"
            },
            "shape": {
                "type": "circle",
                "stroke": {
                    "width": 0,
                    "color": "#000000"
                },
                "polygon": {
                    "nb_sides": 5
                }
            },
            "opacity": {
                "value": 1,
                "random": false,
                "anim": {
                    "enable": false,
                    "speed": 1,
                    "opacity_min": 0.1,
                    "sync": false
                }
            },
            "size": {
                "value": 6,
                "random": true,
                "anim": {
                    "enable": false,
                    "speed": 40,
                    "size_min": 0.1,
                    "sync": false
                }
            },
            "line_linked": {
                "enable": true,
                "distance": 150,
                "color": "#009900",
                "opacity": 0.9,
                "width": 2
            },
            "move": {
                "enable": true,
                "speed": 6,
                "direction": "none",
                "random": false,
                "straight": false,
                "out_mode": "out",
                "bounce": false,
                "attract": {
                    "enable": false,
                    "rotateX": 600,
                    "rotateY": 1200
                }
            }
        },
        "interactivity": {
            "detect_on": "canvas",
            "events": {
                "onhover": {
                    "enable": false,
                    "mode": "grab"
                },
                "onclick": {
                    "enable": false,
                    "mode": "repulse"
                },
                "resize": true
            },
            "modes": {
                "grab": {
                    "distance": 140,
                    "line_linked": {
                        "opacity": 1
                    }
                },
                "bubble": {
                    "distance": 400,
                    "size": 40,
                    "duration": 2,
                    "opacity": 8,
                    "speed": 3
                },
                "repulse": {
                    "distance": 200,
                    "duration": 0.4
                },
                "push": {
                    "particles_nb": 4
                },
                "remove": {
                    "particles_nb": 2
                }
            }
        },
        "retina_detect": true
    });
});
/*
var count_particles, stats, update;
stats = new Stats();
stats.setMode(0);
stats.domElement.style.position = "absolute";
stats.domElement.style.left = "0px";
stats.domElement.style.top = "0px";
document.body.appendChild(stats.domElement);
count_particles = document.querySelector(".js-count-particles");
update = function() {
  stats.begin();
  stats.end();
  if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
    count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
  }
  requestAnimationFrame(update);
};
requestAnimationFrame(update);
*/
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX3JlZ2lzdGVyLmpzIiwiYWJfcGFydGljbGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNoREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhY19yZWdpc3Rlcl9wYWdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkgeyBcbiAgICBcbiAgICBcblxuICAgICQoJyNyZWdpc3RyYXRpb25fZm9ybScpLnZhbGlkYXRlKHsvLyBpbml0aWFsaXplIHRoZSBwbHVnaW5cbiAgICAgICAgcnVsZXM6IHtcblxuICAgICAgICAgICAgXG4gICAgICAgICAgICB2ZXJpZnlfZW1haWxfbmFtZV9uYW1lOlxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGVtYWlsOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgZXF1YWxUbzogXCIjZW1haWxfbmFtZV9pZFwiXG5cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHBhc3N3b3JkX25hbWU6XG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWlubGVuZ3RoOiAzXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB2ZXJpZnlfcGFzc3dvcmRfbmFtZTpcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5sZW5ndGg6IDMsXG4gICAgICAgICAgICAgICAgICAgICAgICBlcXVhbFRvOiBcIiNwYXNzd29yZF9pZFwiXG4gICAgICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGZpcnN0X25hbWVfbmFtZTpcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5sZW5ndGg6IDNcbiAgICAgICAgICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgbGFzdF9uYW1lX25hbWU6XG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWlubGVuZ3RoOiAzXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pO1xuICAgIFxuICAgICBcblxufSk7ICBcblxuICAgIFxuXG5cbiIsIiBcbi8vLm91dGVySGVpZ2h0KHRydWUpIFxuJCh3aW5kb3cpLm9uKCdsb2FkJywgZnVuY3Rpb24gKCkgeyBcbiAgICBcbiAgICBcbiAgICAgICB2YXIgaGVpZ2h0ID0gJChcIiNyZWdpZFwiKS5vdXRlckhlaWdodCh0cnVlKTtcbiAgJChcIiNwYXJ0aWNsZXMtanNcIikuY3NzKFwiaGVpZ2h0XCIsIGhlaWdodCApOyAgXG5cbiAgICBcblxuXG4gIFxuICAgIC8vIFBhcnRpY2xlc0pTIENvbmZpZy5cbnBhcnRpY2xlc0pTKFwicGFydGljbGVzLWpzXCIsIHtcbiAgXCJwYXJ0aWNsZXNcIjoge1xuICAgIFwibnVtYmVyXCI6IHtcbiAgICAgIFwidmFsdWVcIjogODAsXG4gICAgICBcImRlbnNpdHlcIjoge1xuICAgICAgICBcImVuYWJsZVwiOiB0cnVlLFxuICAgICAgICBcInZhbHVlX2FyZWFcIjogNDAwXG4gICAgICB9XG4gICAgfSxcbiAgICBcImNvbG9yXCI6IHtcbiAgICAgIFwidmFsdWVcIjogXCIjZmY4YzAwXCJcbiAgICB9LFxuICAgIFwic2hhcGVcIjoge1xuICAgICAgXCJ0eXBlXCI6IFwiY2lyY2xlXCIsXG4gICAgICBcInN0cm9rZVwiOiB7XG4gICAgICAgIFwid2lkdGhcIjogMCxcbiAgICAgICAgXCJjb2xvclwiOiBcIiMwMDAwMDBcIlxuICAgICAgfSxcbiAgICAgIFwicG9seWdvblwiOiB7XG4gICAgICAgIFwibmJfc2lkZXNcIjogNVxuICAgICAgfVxuICAgIH0sXG4gICAgXCJvcGFjaXR5XCI6IHtcbiAgICAgIFwidmFsdWVcIjogMSxcbiAgICAgIFwicmFuZG9tXCI6IGZhbHNlLFxuICAgICAgXCJhbmltXCI6IHtcbiAgICAgICAgXCJlbmFibGVcIjogZmFsc2UsXG4gICAgICAgIFwic3BlZWRcIjogMSxcbiAgICAgICAgXCJvcGFjaXR5X21pblwiOiAwLjEsXG4gICAgICAgIFwic3luY1wiOiBmYWxzZVxuICAgICAgfVxuICAgIH0sXG4gICAgXCJzaXplXCI6IHtcbiAgICAgIFwidmFsdWVcIjogNixcbiAgICAgIFwicmFuZG9tXCI6IHRydWUsXG4gICAgICBcImFuaW1cIjoge1xuICAgICAgICBcImVuYWJsZVwiOiBmYWxzZSxcbiAgICAgICAgXCJzcGVlZFwiOiA0MCxcbiAgICAgICAgXCJzaXplX21pblwiOiAwLjEsXG4gICAgICAgIFwic3luY1wiOiBmYWxzZVxuICAgICAgfVxuICAgIH0sXG4gICAgXCJsaW5lX2xpbmtlZFwiOiB7XG4gICAgICBcImVuYWJsZVwiOiB0cnVlLFxuICAgICAgXCJkaXN0YW5jZVwiOiAxNTAsXG4gICAgICBcImNvbG9yXCI6IFwiIzAwOTkwMFwiLFxuICAgICAgXCJvcGFjaXR5XCI6IDAuOSxcbiAgICAgIFwid2lkdGhcIjogMlxuICAgIH0sXG4gICAgXCJtb3ZlXCI6IHtcbiAgICAgIFwiZW5hYmxlXCI6IHRydWUsXG4gICAgICBcInNwZWVkXCI6IDYsXG4gICAgICBcImRpcmVjdGlvblwiOiBcIm5vbmVcIixcbiAgICAgIFwicmFuZG9tXCI6IGZhbHNlLFxuICAgICAgXCJzdHJhaWdodFwiOiBmYWxzZSxcbiAgICAgIFwib3V0X21vZGVcIjogXCJvdXRcIixcbiAgICAgIFwiYm91bmNlXCI6IGZhbHNlLFxuICAgICAgXCJhdHRyYWN0XCI6IHtcbiAgICAgICAgXCJlbmFibGVcIjogZmFsc2UsXG4gICAgICAgIFwicm90YXRlWFwiOiA2MDAsXG4gICAgICAgIFwicm90YXRlWVwiOiAxMjAwXG4gICAgICB9XG4gICAgfVxuICB9LFxuICBcImludGVyYWN0aXZpdHlcIjoge1xuICAgIFwiZGV0ZWN0X29uXCI6IFwiY2FudmFzXCIsXG4gICAgXCJldmVudHNcIjoge1xuICAgICAgXCJvbmhvdmVyXCI6IHtcbiAgICAgICAgXCJlbmFibGVcIjogZmFsc2UsXG4gICAgICAgIFwibW9kZVwiOiBcImdyYWJcIlxuICAgICAgfSxcbiAgICAgIFwib25jbGlja1wiOiB7XG4gICAgICAgIFwiZW5hYmxlXCI6IGZhbHNlLFxuICAgICAgICBcIm1vZGVcIjogXCJyZXB1bHNlXCJcbiAgICAgIH0sXG4gICAgICBcInJlc2l6ZVwiOiB0cnVlXG4gICAgfSxcbiAgICBcIm1vZGVzXCI6IHtcbiAgICAgIFwiZ3JhYlwiOiB7XG4gICAgICAgIFwiZGlzdGFuY2VcIjogMTQwLFxuICAgICAgICBcImxpbmVfbGlua2VkXCI6IHtcbiAgICAgICAgICBcIm9wYWNpdHlcIjogMVxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgXCJidWJibGVcIjoge1xuICAgICAgICBcImRpc3RhbmNlXCI6IDQwMCxcbiAgICAgICAgXCJzaXplXCI6IDQwLFxuICAgICAgICBcImR1cmF0aW9uXCI6IDIsXG4gICAgICAgIFwib3BhY2l0eVwiOiA4LFxuICAgICAgICBcInNwZWVkXCI6IDNcbiAgICAgIH0sXG4gICAgICBcInJlcHVsc2VcIjoge1xuICAgICAgICBcImRpc3RhbmNlXCI6IDIwMCxcbiAgICAgICAgXCJkdXJhdGlvblwiOiAwLjRcbiAgICAgIH0sXG4gICAgICBcInB1c2hcIjoge1xuICAgICAgICBcInBhcnRpY2xlc19uYlwiOiA0XG4gICAgICB9LFxuICAgICAgXCJyZW1vdmVcIjoge1xuICAgICAgICBcInBhcnRpY2xlc19uYlwiOiAyXG4gICAgICB9XG4gICAgfVxuICB9LFxuICBcInJldGluYV9kZXRlY3RcIjogdHJ1ZVxufSk7ICBcbiAgfSk7XG4gXG5cbi8qXG52YXIgY291bnRfcGFydGljbGVzLCBzdGF0cywgdXBkYXRlO1xuc3RhdHMgPSBuZXcgU3RhdHMoKTtcbnN0YXRzLnNldE1vZGUoMCk7XG5zdGF0cy5kb21FbGVtZW50LnN0eWxlLnBvc2l0aW9uID0gXCJhYnNvbHV0ZVwiO1xuc3RhdHMuZG9tRWxlbWVudC5zdHlsZS5sZWZ0ID0gXCIwcHhcIjtcbnN0YXRzLmRvbUVsZW1lbnQuc3R5bGUudG9wID0gXCIwcHhcIjtcbmRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoc3RhdHMuZG9tRWxlbWVudCk7XG5jb3VudF9wYXJ0aWNsZXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmpzLWNvdW50LXBhcnRpY2xlc1wiKTtcbnVwZGF0ZSA9IGZ1bmN0aW9uKCkge1xuICBzdGF0cy5iZWdpbigpO1xuICBzdGF0cy5lbmQoKTtcbiAgaWYgKHdpbmRvdy5wSlNEb21bMF0ucEpTLnBhcnRpY2xlcyAmJiB3aW5kb3cucEpTRG9tWzBdLnBKUy5wYXJ0aWNsZXMuYXJyYXkpIHtcbiAgICBjb3VudF9wYXJ0aWNsZXMuaW5uZXJUZXh0ID0gd2luZG93LnBKU0RvbVswXS5wSlMucGFydGljbGVzLmFycmF5Lmxlbmd0aDtcbiAgfVxuICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUodXBkYXRlKTtcbn07XG5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUodXBkYXRlKTtcbiovIl19
