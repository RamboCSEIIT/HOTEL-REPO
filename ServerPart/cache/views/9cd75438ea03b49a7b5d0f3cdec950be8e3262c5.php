<?php $__env->startSection('test-form'); ?>
 



    <!-- OUR FORM -->
    <form class="form-inline justify-content-center" id="formsub" action="/subs" method="POST" style="padding-left: 15px;padding-right: 15px">



        <!-- EMAIL -->
        
        <div class="input-group">
            <input  type="email" class="company_email_font form-control" size="30" name="email" placeholder="Email Address" required>
            <div class="input-group-btn">
                <button type="submit" class="btn btn-danger company_email_font">   Subscribe <span class="fa fa-arrow-right"></span>  </button>
            </div>
        </div>


  
         

    </form>
      <span id="submsg" class="company_moto_font center" style="color: whitesmoke;background-color:maroon"></span>

 
<?php $__env->stopSection(); ?>


<?php $__env->startSection('form'); ?>
<form class="form-inline justify-content-center" action="#"  onsubmit="return submitForm();" id="myform"  method="POST" style="padding-left: 15px;padding-right: 15px">
    <div class="input-group">
        <input  type="email" class="company_email_font form-control" size="30" placeholder="Email Address" required>
        <div class="input-group-btn">
            <button type="submit" class="btn btn-danger company_email_font">   Subscribe  </button>
        </div>
    </div>
</form>

<?php $__env->stopSection(); ?>



<div   id="jumbotron">


    <div class="jumbotron-fluid text-center">
        
        
        <div class="container-fluid">
    <div class="row">
      
        <div class="col-md-12" style="overflow: hidden">
            
             
          
            <div id="cloud1" >
                       <img src="02_IMAGES/aa_HOME_PAGE/03_jumbotron/cloud1.png" alt="Banasura"  class="img-fluid" >
                        
            </div>
          
            <div class="carousel-caption">
              <div class=" company_caption_font " >
                      
                     <p><a style="text-decoration: none">
                      Company Name
                </a></p>  
                      
                    
                  
                  
                  </div>
            </div>
          
         </div>
            
      
      
 </div>
  
  
</div>
  
      


        


    </div>
</div>    
<!-- Container (Services Section) -->
<div id="icons">
    
    <div class="marquee">
        <br>
 
        <p><b> Site design is being getting updated. More options will be available in few days </b> </p>
    </div>
    <div class="container-fluid text-center">
        <div class="row">
            <br>
            <br>
        </div>
        <h2 class = "icon_font">SERVICES</h2>

        <br>
        <div class="row">
            <div class="col-sm-4">
                <i class="fa fa-columns" style="font-size:48px;color:red"></i>
                <h4 class="">HOTELS</h4>
                <p> We provide hotel booking as per the customer requirement. You can book through our site or via directly contacting our team</p>
            </div>
            <div class="col-sm-4">
                <i class="fa fa-text-width" style="font-size:48px;color:red"></i>
                <h4 class="">TOUR GUIDING</h4>
                <p> Experienced tour guide to assist your trip</p>
            </div>
            <div class="col-sm-4">
                <i class="fa fa-paragraph" style="font-size:48px;color:red"></i>
                <h4 class="">CUSTOMER CARE</h4>
                <p> We offer service which ensures customer satisfaction. Our team will be available any time including day and night on request. </p>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-sm-4">
                <i class="fa fa-strikethrough" style="font-size:48px;color:red"></i>
                <h4 class="">SECURITY</h4>
                <p>Security of our customer is utmost priority to us. We make sure that apt people will be available at your service on your trip. Proper monitoring of the hotel and travel amenities is done prior to the trip and also during the tour. </p>
            </div>
            
            
             <div class="col-sm-4">
                <i class="fa fa-undo" style="font-size:48px;color:red"></i>
                <h4 class="company_moto_font">Medical Emergency </h4>
                <p> In case of medical emergencies caused by health issues we will assist you to get the best medical facilities in Wayanad . You will be provided with our medical assistance any time </p>
            </div>
           
            
            <div class="col-sm-4">
                <i class="fa fa-table" style="font-size:48px;color:red"></i>

                <h4 class="">TRANSPORTATION</h4>
                <p> It can be arranged before or after the arrival of the tour. We will arrange travel amenities with properly maintained vechicles as your security is utmost concern for us</p>
            </div>
        </div>
    </div>

</div>


<?php $__env->startSection('image_wyn'); ?> 





<?php $__currentLoopData = $galleryWayanad; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $gallery_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>




<li>
    <a href="">  <img  src="<?php echo $gallery_item->image_link; ?>"  alt=""/> </a>  
</li>





<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('img_dots'); ?> 


<div id="nav-dots" class="nav-dots">


<?php $__currentLoopData = $galleryWayanad; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $gallery_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<?php if($index==0): ?>
<span class="nav-dot-current"></span>
<?php else: ?>
<span></span>

<?php endif; ?>



<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>

<?php $__env->stopSection(); ?> 

<h2 class="display-5 text-center company_caption_font"> Welcome to Wayanad</h2>

    <div  class="wrapper">
      <ul id="sb-slider" class="sb-slider">
               <?php echo $__env->yieldContent('image_wyn'); ?>                    

       </ul>
        
      <div id="shadow" class="shadow"></div>
      
      <div id="nav-arrows" class="nav-arrows">
        <a href="#">Next</a>
        <a href="#">Previous</a>
      </div>
      <div id="nav-dots" class="nav-dots">
        <span class="nav-dot-current"></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>

    








    

<?php $__env->startSection('image_prl'); ?> 





<?php $__currentLoopData = $galleryProfile; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $gallery_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


<div class="item"> 
    <img src="<?php echo $gallery_item->image_link; ?>" alt="Los Angeles" class="img-fluid" >
</div>








<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php $__env->stopSection(); ?> 

     
<div id="profile" style="margin-top: 10px">


        <h2 class="font_profile text-center"> Company Name</h2><br>
        <div class="container mt-5">


            <div class="row">

                <div class="owl-carousel owl-theme">                    
                    <?php echo $__env->yieldContent('image_prl'); ?>                    

                </div>
            </div> 
            <div class="row">
                <br>
                <br>

            </div>


        </div>




    </div>


 
 <?php $__env->startSection('image_pkg'); ?> 





<?php $__currentLoopData = $galleryPackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $gallery_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 


<div class="item" style=""> 
    
    <div  class="card_model card_v">
         <div class="container">
        <div class="front" style="background-image: url(<?php echo $gallery_item->image_link; ?>)">
          <div class="inner">
            <p><?php echo $gallery_item->heading; ?></p>
             
          </div>
                                       <div class="text-center mb-3"><a class="card-link btn btn-primary">Price Details</a></div>

        </div>
        <div class="back">
            <div class="inner" >
      <table class="table" style="height:inherit;border-radius:20%;">
    <thead >
      <tr style="background:#ff0081;" class="text-center">
          <th class="font_heading " style="color:whitesmoke"><?php echo $gallery_item->heading; ?></th>
     </tr>
    </thead>
    <tbody style="color:brown">
        
         
      <tr class="table-warning">
        <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-12">
                          <?php echo $gallery_item->subheading; ?>

                    </div>
                     
   
                </div>
                
            </div>
         
        
        </td>
        
        
        
      </tr>
      
     <tr class="table-warning">
        <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-12">
                                             <?php if( $gallery_item->tentative==1): ?>
                           <div class="text-center mb-3"><a class="card-link btn btn-primary">Cost Tentative</a></div>
                       <?php else: ?>
                           
                           <div class="text-center mb-3"><a class="card-link btn btn-primary"><span>&#8377;</span> <?php echo $gallery_item->cost; ?> <?php echo $gallery_item->payment_mode; ?> </a></div>
                            
                       <?php endif; ?>
                    </div>

   
                </div>
                
            </div>
         
        
        </td>
        
        
        
      </tr>
        
    </tbody>
  </table>
  
              
           </div>
            
            
         </div>
      </div>

    </div>
  
</div>








<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


<?php $__env->stopSection(); ?> 



<div id="package" class="mt-5">
    <h2 class= "text-center"> <span class="font_package_heading"> Packages</span></h2><br>
    
    
         <div class="container mt-1">


            <div class="row">

                <div class="owl-carousel owl-theme">                    
                    <?php echo $__env->yieldContent('image_pkg'); ?>                    

                </div>
            </div> 
              
             
              <div class="row justify-content-center">
    <div class="col-12 text-center">
        <button id="button_pkg" class="bubbly-button">Booking & Package Details</button>

      
    </div>
      </div>


        </div>

    


</div>
 
 
<?php $__env->startSection('contactForm'); ?>

<form    method="POST" id="enquiryform" action="/contact" style="padding:0px 15px;">

    <div class="row">
        <div class="col-sm-12 form-group">
            <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 form-group">
            <input class="form-control" id="mobile" name="mobile" placeholder="mobile" type="number" required>
        </div>
        <div class="col-sm-6 form-group">
            <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
    </div>


    <textarea class="form-control" id="comments" name="comments"  placeholder="Comment" rows="5"></textarea><br>

    <div class="row">
        <div class="col-sm-12 form-group text-center">

            <button class="btn btn-default     centered button_round   " type="submit">Send</button>

        </div>
    </div>

</form>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('Weather'); ?>
<div class="jumbotron">
    
  <table class="table">
    <thead>
      <tr style="background:#ff0081;" class="text-center">
          <th class="font_heading " style="color:whitesmoke">Weather Forecast</th>
     </tr>
    </thead>
    <tbody style="color:brown">
        
      <tr class="table-danger ">
          <td class="text-center">
          
          <div id="date"></div>
        </td>
    
      </tr>
        
      <tr class="table-warning">
        <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                         <i class="fa fa-home" style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                       Company Name, Wayanad
                    </div>

                </div>
                
            </div>
         
        
        </td>
        
        
        
      </tr>
      
      
      <tr class="table-warning">
              <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                           <i class="wi wi-fog" style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                       <span id="weather-id" style=""></span>
                    </div>

                </div>
                
            </div>
         
        
        </td>
    
      </tr>
      
      
       <tr class="table-warning">
              <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                         <i class="wi wi-hot" style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                        Temperature: <span id="temperature"></span> &deg;<span id="toCelsius">C</span> 
 
                    </div>

                </div>
                
            </div>
         
        
        </td>
    
      </tr>  
      
       <tr class="table-warning">
              <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                         <i class="wi wi-windy " style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                        Wind Speed:   <span id="wind-speed"></span> 
                    </div>

                </div>
                
            </div>
         
        
        </td>
    
      </tr>  
      
<tr class="table-warning">
              <td>
            
            <div class="container">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                         <i class="wi wi-humidity" style="font-size:48px;color:brown"></i>
                    </div>

                    <div class="col-8">
                        Humidity :   <span id="humidity"></span> 
                    </div>

                </div>
                
            </div>
         
        
        </td>
    
      </tr>  
      
    </tbody>
  </table>
   
       
    
  
    
    
    
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('contact'); ?>
               <div class="row  justify-content-center mt-5" >
                    <div class="col-12">
                        
                        <h2 class="text-center">CONTACT</h2> 
                                                <div class="text-center">      <span id="contactmsg" class="company_moto_font " style="color: whitesmoke;background-color:maroon;font-size: 15px"></span>

                        </div>

 
                        <?php echo $__env->yieldContent('contactForm'); ?> 

                    </div>
                </div>

<?php $__env->stopSection(); ?>


<div id="contact" >



    <!-- Container (Contact Section) -->
    <div class="container" >


        <div class="row " >
            <div class="col-lg-5" style="padding-left: 20px;padding-right: 20px"  >
                <?php echo $__env->yieldContent('Weather'); ?>
            </div>

            <div class="col-lg-7 jumbotron boarderLine " style="padding-left: 25px;padding-right: 25px" >
                
                     <?php echo $__env->yieldContent('contact'); ?>
                
                

 
            </div>

        </div>


    </div>


</div>


<!-- Add Google Maps -->
 
        
  

<div class="map-responsive">
    <div id="map" style="height:400px;width:100%;" ></div>    
</div>
    <footer id="myFooter">
        <div class="container ">
            <div class="row">
                <div class="col-md-4 ">
                    <h5>Current Page</h5>
                    <ul>
                        <li><a href="#topnavid" >Page Top</a></li>
                        <li><a href="#wayanad">Wayanad</a></li>
                        <li><a href="#profile">Our Resort</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li><a href="#package">Packages</a></li>
                    </ul>
                </div>
                  
                 
                <div class="col-md-4">
                    <h5>Info</h5>
                    <ul>
                        
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Login</a></li>
                         <li><a href="#">Register</a></li>

                    </ul>
                </div>
                
                 
                
               
                 
                 
                
                <div class="col-md-4">
                    <h5>Address</h5>
                    <ul>
                       <li> <a href="#">  Address </a> </li>
                        <li> <a href="#">  Address </a> </li>
                        <li> <a href="#">  Wayanad Kerala,673575 </a> </li>
                        <li> <a href="#">  ph:+91-8129443182</a> </li>
                        <li> <a href="#">  ph:+91-9778867855</a> </li>
                        
                        <li> <a href="#">  a@a.com </a> </li>
                    </ul>
                </div>
            </div>
            <!-- Here we use the Google Embed API to show Google Maps. -->
            <!-- In order for this to work in your project you will need to generate a unique API key.  -->
 
        </div>
        <div class="social-networks">
            <a href="https://www.facebook.com/WAYANADTOURSANDTRAVELSS/" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="https://www.facebook.com/WAYANADTOURSANDTRAVELSS/" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="https://www.facebook.com/WAYANADTOURSANDTRAVELSS/" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
             
            <p>© 2017 Copyright Text </p>
        </div>
    </footer>