 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Important Tourist Places In Wayanad</title>

    <link rel="stylesheet" href="https://technology-ebay-de.github.io/working-at-mobilede/css/reveal.css">
    <link rel="stylesheet" href="https://technology-ebay-de.github.io/working-at-mobilede/css/theme/pahund.css">

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="https://technology-ebay-de.github.io/working-at-mobilede/lib/css/zenburn.css">

    <!-- Printing and PDF exports -->
    <script>
        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = window.location.search.match(/print-pdf/gi) ? 'css/print/pdf.css' : 'css/print/paper.css';
        document.getElementsByTagName('head')[0].appendChild(link);
    </script>
