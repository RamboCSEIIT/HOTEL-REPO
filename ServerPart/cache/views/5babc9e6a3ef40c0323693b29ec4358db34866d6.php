<?php $__env->startSection('user_nav'); ?> 

               <li  id ="logout-page" class="nav-item">

                    <a class="nav-link " href="/logout"> <i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                </li>


<?php $__env->stopSection(); ?> 


<?php $__env->startSection('general_nav'); ?> 

                <li  id ="login-page" class="nav-item">

                    <a class="nav-link " href="/login"> <i class="fa fa-sign-in" aria-hidden="true"></i>Login</a>
                </li>

<?php $__env->stopSection(); ?> 

<?php $__env->startSection('admin_nav'); ?> 

  
 
                <li  id ="dashboard-page" class="nav-item" >

                    <a class="nav-link " href="/admin-panel">  Admin Panel</a>
                </li>

 
                <li  id ="logout-page" class="nav-item">

                    <a class="nav-link " href="/logout"> <i class="fa fa-sign-out" aria-hidden="true"></i>  Logout Admin</a>
                </li>


<?php $__env->stopSection(); ?> 
 
<div id="topnavid">

    <nav id = "myScrollspy" class="navbar navbar-expand-sm  fixed-top" >
        <!-- 
       style="background-color: #f4511e;"
          <a class="navbar-brand" href="#">Navbar</a>
         <li id ="logout-page"><a href="/logout"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>Logout</a></li>

        -->


        <div class="navbar-brand" style="color:whitesmoke" >    
            <div class="text-center">
                <img class="logo img-circle" src="/02_IMAGES/favicon.png" alt="Logo" width="30" height="30">

            </div>



        </div>  


        <button class="navbar-toggler"  type="button" data-toggle="collapse" data-target="#collapsibleNavbar" style="margin:10px;color:whitesmoke">
            <span class="icon" >  <i class="fa fa-bars" id="tag" aria-hidden="true">Menu</i></span>
        </button>



        <div class="collapse navbar-collapse" id="collapsibleNavbar">


            <ul class="navbar-nav navbar-right mr-auto company_email_font">


                <li  id ="home-page" class="nav-item">
                    <a class="nav-link" href="/">Home</a>
                </li>
                
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  
                                        
                                            <span class="">Page Navigation</span>
                                        
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink" id="pagenav">
                                        <a class="dropdown-item " id="sub_nav1" href="#package">Packages</a>
                                        <a class="dropdown-item " id="sub_nav2" href="#contact">Enquiry</a>
                                        <a class="dropdown-item " id="sub_nav3" href="#profile">Homestay</a>
 
                                    </div>
                                </li>


                
                <li  id ="about-tour-page" class="nav-item">
                    <a class="nav-link  " href="/about-tour">About</a>
                </li>
                
                <li id ="register-page" class="nav-item">
                    <a class="nav-link" href="/register">Register</a>
                </li>   
                
                
                 <li id ="tour-places" class="nav-item">
                    <a class="nav-link " href="/package">Booking</a>
                </li> 

                
      
            </ul>



            <ul class="navbar-nav ml-auto">

                <?php if(tour\auth\LoggedIn::user() && tour\auth\LoggedIn::user()[0]->access_level == 2): ?>
                     <?php echo $__env->yieldContent('admin_nav'); ?>
                <?php elseif(tour\auth\LoggedIn::user()): ?>      
                     <?php echo $__env->yieldContent('user_nav'); ?>
                <?php else: ?>   
                     <?php echo $__env->yieldContent('general_nav'); ?>
                <?php endif; ?>

            </ul>

         </div>  
    </nav>


</div> 
