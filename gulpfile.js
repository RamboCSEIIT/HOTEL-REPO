var SASS_CHECK_PATH                                          = '00_SASS/**/*.*';
var SCRIPTS_CHECK_PATH                                    = '01_SCRIPTS/**/*.*';
 
var PHP_PATH_BOOT                                       = 'BootingFiles/**/*.*';
var PHP_PATH_SRC                                    = 'ServerPart/src/**/*.php';
var VIEW_T_WORK_SPACE              = 'ViewsT/aa_ServerPart/aa_WorkSpace/**/*.*';
var VIEW_PATH_FRAGMENT                           = 'ViewsT/ab_Fragments/**/*.*';
 
var VALID =1;
var IGNORE =0;
var gulp = require('gulp');
 

 
global.printMsg = false;


var gulpSequence = require('gulp-sequence');
// process.env.NODE_ENV
 var util = require('gulp-util');






//Directory Structire
var requireDir       = require('require-dir');
var common_tasks     = requireDir('./zz_tasks/zz_COMMON_TASKS');
var env_tasks        = requireDir('./zz_tasks/zz_ENV_VARIABLES');
var webpage_tasks    = requireDir('./zz_tasks/aa_WEBPAGES');
var core_tasks       = requireDir('./zz_tasks');









//global variable
global.browserSync = require('browser-sync').create();

//Common tasks
gulp.task('clear'             , common_tasks.b_miscellaneous.clear);
gulp.task('testG'             , common_tasks.c_clean.test_global);

gulp.task('browser-init'      , common_tasks.a_browser.browser_init);
gulp.task('browser-reload'    , common_tasks.a_browser.browser_reload);
gulp.task('javaScriptError'   , core_tasks.a_javascript.javaScriptError);


///Environment variables
gulp.task('set-mode-dev-env'    , env_tasks.common_env.set_mode_dev_env);
gulp.task('set-mode-deploy-env' , env_tasks.common_env.set_mode_deploy_env);

 
  
 // miscellaneous
gulp.task('clean-without-image' , common_tasks.c_clean.clean_without_image);
gulp.task('clean-with-image'    , common_tasks.c_clean.clean_with_image);

//Common specific
gulp.task('javascript-move'     , core_tasks.a_javascript.javascript_move);
gulp.task('sass-move'           , core_tasks.b_sass.sass_move);
gulp.task('html5-body-move'     , core_tasks.c_html.html5_body_move);
gulp.task('html5-minify-view'   , core_tasks.c_html.html5_minify_view);
gulp.task('image-move'          , core_tasks.d_images.image_move); 



///// NEW PAGE ADJUST TASKS ////////////////////////////////////////////////////////////////////////
//web pages
gulp.task('home-page-mode' , webpage_tasks.aa_set_homepage_env.home_page_mode);
gulp.task('login-page-mode' , webpage_tasks.ab_set_loginpage_env.login_page_mode);
gulp.task('register-page-mode' , webpage_tasks.ac_set_registerpage_env.register_page_mode);
gulp.task('package-page-mode' , webpage_tasks.ad_set_packagepage_env.package_page_mode);
gulp.task('admin-page-mode' , webpage_tasks.ae_set_adminpage_env.admin_page_mode);
gulp.task('tourplaces-page-mode' , webpage_tasks.af_set_tourplacespage_env.tourplaces_page_mode);
gulp.task('booking-page-mode' , webpage_tasks.ag_set_bookingpage_env.booking_page_mode);
gulp.task('crud-page-mode' , webpage_tasks.zd_curd_page_set_env.zd_curd_page_mode);




process.env.TEST_PAGE  = "localhost";

global.items = [
  ['home-page-mode'          , VALID],
  ['login-page-mode'         , VALID],
  ['register-page-mode'      , VALID],
  ['package-page-mode'       , VALID],
  ['admin-page-mode'         , VALID],
  ['tourplaces-page-mode'    , VALID],
  ['booking-page-mode'       , VALID],
  ['crud-page-mode'          , VALID]
 
  
];
/////////////////////////////////////////////////////////////////////////////////////////




 
 
gulp.task('pages',function (cb)
{
    initialize_page_tasks();

   // console.log(global.task_pages_sass_js_body);

    gulpSequence(global.task_pages_sass_js_body,'html5-minify-view')(cb);

    
});

 

gulp.task('pages-watch',function (cb)
{
 
     
    gulpSequence('pages','browser-reload')(cb);

    
});

 

////////// WATCH AREA //////////////

 

 


 gulp.task('watch', function ( )
{
    console.log("Inside XXXXXXXXXXXXXXXXXXXXXXXXX");
  
    gulp.watch([PHP_PATH_BOOT,PHP_PATH_SRC], function (  )
    {

        gulp.start('browser-reload');


    });
 
   
    gulp.watch([SASS_CHECK_PATH,SCRIPTS_CHECK_PATH,VIEW_T_WORK_SPACE,VIEW_PATH_FRAGMENT], function ()
    {

         //  console.log("Inside YYYYYYYYYYYYYYYYYYYYYYYYYY");
           
                 gulp.start('pages-watch');


 

    });

  
     
    
 




    return;
});


 gulp.task('dev', function (cb)
{

    gulpSequence( 
    'clear',
    'clean-without-image',
    'set-mode-dev-env',
    'pages',
    'browser-init',
    'watch')(cb);

    
});


gulp.task('dep', function (cb)
{

    gulpSequence( 
    'clear',
    'clean-without-image',
    'set-mode-deploy-env',
    'pages',
    'browser-init',
    'watch')(cb);

    
});



 

 
 
 gulp.task('temp', function (cb)
{

    var options_script = {
                    SCRIPTS_OUT_PATH: util.env.name,
                    SCRIPTS_IN_PATH: util.env.name,
                    SCRIPTS_OUT_FILE_NAME: util.env.name
                };

    return;
});
//gulp make-page  --name "az_curd_page"
gulp.task('make-page', function() 
{
    var DNAME = (util.env.name).toUpperCase();
 
var options_script_page = {
                  
                    SASS_IN:"000_TEMPLATE/00_SASS" ,
                    SASS_OUT:"00_SASS/"+DNAME ,
                    SCRIPTS_IN:"000_TEMPLATE/01_SCRIPTS" ,
                    SCRIPTS_OUT:"01_SCRIPTS/"+DNAME ,
                    IMAGES_IN:"000_TEMPLATE/02_IMAGES" ,
                    IMAGES_OUT:"02_IMAGES/"+DNAME,
                    WORKSPACE_IN:"000_TEMPLATE/03_WORK_SPACE" ,
                    WORKSPACE_OUT:"ViewsT/aa_ServerPart/aa_WorkSpace/"+DNAME,
                    FRAGMENTS_IN:"000_TEMPLATE/04_FRAGMENTS" ,
                    FRAGMENTS_OUT:"ViewsT/ab_Fragments/"+DNAME,
                    
                    GULP_IN:"000_TEMPLATE/05_GULP_PAGE" ,
                    GULP_OUT:"zz_tasks/aa_WEBPAGES",   
                    
                    PHP_IN:"000_TEMPLATE/06_PHP_PAGE_CLASS" ,
                    PHP_OUT:"ServerPart/src/Controllers",   
                    
                    
                   'NAME':util.env.name,
                   'DNAME':DNAME
                    
                    
                };

         return make_template(options_script_page);       
            
});
//gulp del-page  --name "az_curd_page"



gulp.task('del-page', function() 
{
    
var DNAME = (util.env.name).toUpperCase();
 
var options_script_page = {
                  
                    SASS_OUT:"00_SASS/"+DNAME ,
                    SCRIPTS_OUT:"01_SCRIPTS/"+DNAME ,
                    IMAGES_OUT:"02_IMAGES/"+DNAME,
                    WORKSPACE_OUT:"ViewsT/aa_ServerPart/aa_WorkSpace/"+DNAME,
                    FRAGMENTS_OUT:"ViewsT/ab_Fragments/"+DNAME,
                    GULP_OUT:"zz_tasks/aa_WEBPAGES",   
                    PHP_OUT:"ServerPart/src/Controllers",   
                    
                    
                   'NAME':util.env.name,
                   'DNAME':DNAME
                    
                    
                };
                
                
                        return remove_template(options_script_page);       


 
            
});



 
