
 

module.exports =
        {
             
            browser_init: function ()
            {

                global.browserSync.init(
                        {
                            proxy:
                                    {
                                        target: process.env.TEST_PAGE, // can be [virtual host, sub-directory, localhost with port]
                                        //    ws: true // enables websockets
                                        // target: "localhost/register"
                                    }
                        }
                );

            }
            ,
            browser_reload: function ()
            {

                global.browserSync.reload();


            }



        };