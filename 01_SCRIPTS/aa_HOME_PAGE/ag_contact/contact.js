// magic.js
$(document).ready(function () {

    // process the form
    $('#contactId').submit(function (event) {

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'ame': $('input[name=name]').val(),
            'email': $('input[name=email]').val(),
            'comments': $('input[name=comments]').val()
        };

        // process the form
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: '/contact', // the url where we want to POST
            data: formData, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            encode: true
        })
                // using the done promise callback
                .done(function (data) {

                    // log data to the console so we can see
                    console.log(data);

                    // here we will handle errors and validation messages
                    if (!data.success)
                    {

                        $("#contactmsg").html(data.errors.email);
                        /*                        // handle errors for name ---------------
                         if (data.errors.name) {
                         $('#name-group').addClass('has-error'); // add the error class to show red input
                         $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                         }
                         
                         // handle errors for email ---------------
                         if (data.errors.email) {
                         $('#email-group').addClass('has-error'); // add the error class to show red input
                         $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                         }
                         
                         // handle errors for superhero alias ---------------
                         if (data.errors.superheroAlias) {
                         $('#superhero-group').addClass('has-error'); // add the error class to show red input
                         $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                         }*/

                    } else {

                        // ALL GOOD! just show the success message!
                        // $('form').append('<div class="alert alert-success">' + data.message + '</div>');

                        // usually after form submission, you'll want to redirect
                        // window.location = '/thank-you'; // redirect a user to another page
                        //  alert('success'+data.message); // for now we'll just alert the user

                        $("#contactmsg").html(data.message);

                    }

                });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });



    $('#enquiryform').submit(function (event) {

     //  var result = $('#enqid').val();
       var name = $('#name').val();
       var email=$('#email').val();
       var mobile=$('#mobile').val();
       var comments=$('#comments').val();
     
       
       // alert("hai");

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
             'name': name,
             'email': email,
           'comments': comments,
             'mobile'  :mobile
        };
       // console.log("Hello :: "+formData);
       // 
       // 
        // process the form
        
       // alert(formData.name+"::"+formData.email +"::"+formData.mobile+"::"+formData.comments);
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: '/contact', // the url where we want to POST
            data: formData, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            encode: true
        })
                // using the done promise callback
                .done(function (data) {

                    // log data to the console so we can see
                    console.log(data);

                    // here we will handle errors and validation messages
                    if (!data.success)
                    {

                        $("#contactmsg").html(data.message);
                        /*                        // handle errors for name ---------------
                         if (data.errors.name) {
                         $('#name-group').addClass('has-error'); // add the error class to show red input
                         $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                         }
                         
                         // handle errors for email ---------------
                         if (data.errors.email) {
                         $('#email-group').addClass('has-error'); // add the error class to show red input
                         $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                         }
                         
                         // handle errors for superhero alias ---------------
                         if (data.errors.superheroAlias) {
                         $('#superhero-group').addClass('has-error'); // add the error class to show red input
                         $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                         }*/

                    } else {

                        // ALL GOOD! just show the success message!
                        // $('form').append('<div class="alert alert-success">' + data.message + '</div>');

                        // usually after form submission, you'll want to redirect
                        // window.location = '/thank-you'; // redirect a user to another page
                        //  alert('success'+data.message); // for now we'll just alert the user

                        $("#contactmsg").html(data.message);

                    }

                });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });



});


$(document).ready(function() {
  // get location from user's IP address
  
    var latitude = 11.6700;
    var longitude = 75.9578;
    
    
      $("#location").html(
      "Location :  Wayanad "
    );
     
     Date.prototype.yyyymmdd = function() {
  var yyyy = this.getFullYear().toString();
  var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
  var dd  = this.getDate().toString();
  return yyyy + "/" + (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]); // padding
};

var date = new Date();

  $("#date").html(date    );
 
    
  // get weather using OpenWeatherMap API
    $.getJSON(
      "https://cors.5apps.com/?uri=http://api.openweathermap.org/data/2.5/weather?lat=" +
        latitude +
        "&lon=" +
        longitude +
        "&units=metric&APPID=c3e00c8860695fd6096fe32896042eda",
      function(data) {
        var windSpeedkmh = Math.round(data.wind.speed * 3.6);
        var Celsius = Math.round(data.main.temp);
        var iconId = data.weather[0].icon;
        var weatherURL = "http://openweathermap.org/img/w/" + iconId + ".png";

        var iconImg = "<img src='" + weatherURL + "'>";
       // $("#sky-image").html(iconImg);
        $("#weather-id").html("Skies: " + data.weather[0].description);

        $("#temperature").html(Celsius);
        $("#toFahrenheit").click(function() {
          $("#temperature").html(Math.round(9 / 5 * Celsius + 32));
          $("#wind-speed").html(Math.round(windSpeedkmh * 0.621) + " mph");
        });
        $("#toCelsius").click(function() {
          $("#temperature").html(Celsius);
          $("#wind-speed").html(windSpeedkmh + " km/hr");
        });

        $("#wind-speed").html(windSpeedkmh + " km/h");
        $("#humidity").html( data.main.humidity + " %");
        
      }
    );

  /*
  $.getJSON("https://ipinfo.io", function(info) {
    var locString = info.loc.split(", ");
    
    var latitude = parseFloat(locString[0]);
    var longitude = parseFloat(locString[1]);
    $("#location").html(
      "Location: " + info.city + ", " + info.region + ", " + info.country
    );

    // get weather using OpenWeatherMap API
    $.getJSON(
      "https://cors.5apps.com/?uri=http://api.openweathermap.org/data/2.5/weather?lat=" +
        latitude +
        "&lon=" +
        longitude +
        "&units=metric&APPID=c3e00c8860695fd6096fe32896042eda",
      function(data) {
        var windSpeedkmh = Math.round(data.wind.speed * 3.6);
        var Celsius = Math.round(data.main.temp);
        var iconId = data.weather[0].icon;
        var weatherURL = "http://openweathermap.org/img/w/" + iconId + ".png";

        var iconImg = "<img src='" + weatherURL + "'>";
        $("#sky-image").html(iconImg);
        $("#weather-id").html("Skies: " + data.weather[0].description);

        $("#temperature").html(Celsius);
        $("#toFahrenheit").click(function() {
          $("#temperature").html(Math.round(9 / 5 * Celsius + 32));
          $("#wind-speed").html(Math.round(windSpeedkmh * 0.621) + " mph");
        });
        $("#toCelsius").click(function() {
          $("#temperature").html(Celsius);
          $("#wind-speed").html(windSpeedkmh + " km/hr");
        });

        $("#wind-speed").html(windSpeedkmh + " km/h");
        $("#humidity").html("Humidity: " + data.main.humidity + " %");
      }
    );
  });*/
});







