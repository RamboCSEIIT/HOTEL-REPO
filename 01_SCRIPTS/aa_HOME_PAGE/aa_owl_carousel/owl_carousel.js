  
  $('.owl-carousel').owlCarousel({
  
     // CSS Styles
    baseClass : "owl-carousel",
    theme : "owl-theme",
    loop:true,
    autoplay:true,        
    margin:10,
    responsiveClass:true,
    nav:true,
    rewind:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
});

